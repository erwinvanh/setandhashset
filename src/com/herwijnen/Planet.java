package com.herwijnen;

public final class Planet extends HeavenlyBody {
    public Planet(String name, double orbitalPeriod) {
        super(name, orbitalPeriod, BodyTypes.PLANET);
    }

    @Override
    public boolean addSatellite(HeavenlyBody satellite) {
        if (!satellite.getKey().getBodyType().equals(BodyTypes.MOON)) {
            //System.out.println("Only object of type moon allowed");
            return false;
        } else {
            return super.addSatellite(satellite);
        }
    }
}
